# Roll
Roll is a dice rolling cli tool.

## Using
`roll [options] {die}`

### Where:
* {die} = Dice to roll (as many as you want separated by a space) on the format `<n_of_dices>d<sides>`

### Options:
* `-h`, `--help` Help: Shows a help message with a how to use and a list of the options;
* `-s` Simple separation: Separate results between {die}s
* `-S` Total (strong) separation: Separates results of all individual {die}s

## Compiling from source
Use the command:
`mkdir build && cd build && cmake .. && make`.
It will create the ./roll executable that you can then install using the `sudo make install` command or putting it on a `$PATH` directory.

