#include <iostream>
#include <string>
#include <sstream>
#include <random>
#include <vector>

void help()
{
	std::cout << "Roll - easy cli die rolling tool\n"
	<< "usage: roll [options] {die}\n"
	<< "options:\n"
	<< "-h, --help\tShows this message\n"
	<< "-s\t\tSimple separation: Separate results between {die}s\n"
	<< "-S\t\tTotal separation: Separates results of all individual {die}s"
	<< std::endl;
}

int main(int argc, char* argv[])
{
	bool simple_separation = false, total_separation = false;
	int sum = 0;
	std::vector<std::string> rolls;
	std::random_device rd;
	std::default_random_engine dre(rd());

	for(int i = 1; i != argc; ++i)
	{
		if(argv[i][0] == '-')
		{
			if(argv[i][1] == '-')
			{
				std::string option(argv[i]+2);
				if(option == "help")
				{
					help();
					return 0;
				} else
				{
					std::cout << "Option " << option << " not found" << std::endl;
					help();
					return 0;
				}
			} else
			{
				std::string options(argv[i]+1);
				for(auto option : options)
				{
					if(option == 's') simple_separation = true;
					else if(option == 'S') total_separation = true;
					else if(option == 'h')
					{
						help();
						return 0;
					} else
					{
						std::cout << "Option " << option << " not found" << std::endl;
						help();
						return 0;
					}
				}
			}
		} else rolls.emplace_back(argv[i]);
	}

	for(auto arg : rolls)
	{
		int dices, sides;
		std::string dices_raw(arg, 0, arg.find("d"));
		if(dices_raw.empty()) dices = 1;
		else std::stringstream(dices_raw) >> dices;
		std::stringstream(std::string(arg, arg.find("d")+1)) >> sides;

		std::uniform_int_distribution<int> ds(1, sides);

		for(int n = 0; n != dices; ++n)
		{
			sum += ds(dre);
			if(total_separation)
			{
				std::cout << 'd' << sides << ": " << sum << std::endl;
				sum = 0;
			}
		}

		if(!total_separation && simple_separation)
		{
			std::cout << dices << 'd' << sides << ": " << sum << std::endl;
			sum = 0;
		}
	}

	if(!simple_separation && !total_separation)
	{
		std::cout << sum << std::endl;
	}
}

